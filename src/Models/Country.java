/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Controllers.CountryController;

/**
 *
 * @author pearson
 */
public class Country extends GenericModel {
    private String iso_31661;

    public Country(String name, String iso_31661) {
        this.name = name;
        this.iso_31661 = iso_31661;
    }
    
    public Country(String name) {
        this.name = name;
        
        CountryController controller = CountryController.getSharedInstance();
        Country country = controller.getCountryWithName(name);
        iso_31661 = country.iso_31661;
        
    }


    // Métodos de TableModel
    @Override
    public String getSecondColumn() {
        return iso_31661;
    }
    
    // Getters e Setters
    public String getIso_31661() {
        return iso_31661;
    }

    public void setIso_31661(String iso_31661) {
        this.iso_31661 = iso_31661;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
    
}
