/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author pearson
 */
public class Company extends GenericModel {
    private Country country;

    public Company(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    // Método de TableModel
    @Override
    public String getSecondColumn() {
        return country.getName();
    }

    // Getters e Setters
     public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
    
}
