/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Controllers.ActorsController;

/**
 *
 * @author pearson
 */
public final class CastMember extends Actor {
    private String character;

    public CastMember(String name, String character) {
        super(name);
        this.character = character;
    }
    
    // Métodos TableModel 
    @Override
    public String getSecondColumn() {
        return character;
    }

    @Override
    public String getFirstColumn() {
        return name;
    }
    
    // Getters e Setters
    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public void setActor(Actor newActor) {
        for(TableModel object: ActorsController.list) {
            TeamMember registeredActor = (TeamMember) object;
            
            if(registeredActor.getName().equals(newActor.getName())) {
                this.name = newActor.getName();
                return;
            }
        }
        
    }
}
