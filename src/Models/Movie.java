/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author pearson
 */
public final class Movie extends GenericModel {
    private String originalName;
    private String synopsis;
    private Date releaseDate;
    private Gender gender;
    private BigDecimal budget;
    private BigDecimal revenue;
    private long duration;
    private ArrayList<TableModel> companies;
    private ArrayList<TableModel> team;
    private ArrayList<TableModel> cast;

    public Movie(String name, String originalName, String synopsis, String releaseDate, Gender gender, String budget, String revenue, String duration) {
        this.name = name;
        this.originalName = originalName;
        this.synopsis = synopsis;
        this.gender = gender;
        
        setReleaseDate(releaseDate);
        
        setBudget(budget);
        setRevenue(revenue);
        setDuration(duration);
    
        cast = new ArrayList<>();
        team = new ArrayList<>();
        companies = new ArrayList<>();
        
    }
    
    // Métodos úteis
    public String getFormattedBudget() {
        return bigDecimalToString(budget);
    }
    
    public String getFormattedRevenue() {
        return bigDecimalToString(revenue);
    }
    
    public String getFormattedDuration() {
        return duration + " min";
    }
    
    public String getFormattedReleaseDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(releaseDate);
    }
    
    private BigDecimal currencyStringToBigDecimal(String string) {

        try {
            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
            Number number = currencyFormat.parse(string);
            BigDecimal bigDecimail = new BigDecimal(number.toString());
            return bigDecimail;
        } catch (ParseException ex) {
            return null;
        }

    }
    
    private String bigDecimalToString(BigDecimal bigDecimal) {
        // Retornará sempre a moeda local
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        return numberFormat.format(bigDecimal);
    }
    
    

    // Getters e Setters
    @Override
    public String getSecondColumn() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        return formatter.format(releaseDate);
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            this.releaseDate = sdf.parse(releaseDate);
        } catch (ParseException ex) {
            this.releaseDate = null;
        }
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = currencyStringToBigDecimal(budget);
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = currencyStringToBigDecimal(revenue);
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = Long.parseLong(duration);
    }

    public ArrayList<TableModel> getCompanies() {
        return companies;
    }

    public void setCompanies(ArrayList<TableModel> companies) {
        this.companies = companies;
    }

    public ArrayList<TableModel> getTeam() {
        return team;
    }

    public void setTeam(ArrayList<TableModel> team) {
        this.team = team;
    }

    public ArrayList<TableModel> getCast() {
        return cast;
    }

    public void setCast(ArrayList<TableModel> cast) {
        
        this.cast = cast;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        return Objects.equals(this.originalName, other.originalName);
    }

}
