/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author pearson
 */
public class TeamMember extends GenericModel implements TableModel {
    protected Position position;

    public TeamMember(String name, Position position) {
        this.name = name;
        this.position = position;
    }
    
    public TeamMember(String name) {
        this.name = name;
    }
    
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    // Método de TableModel
    @Override
    public String getSecondColumn() {
        return this.getPosition().toString();
    }
    
}
