/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 * Possivel herança
 * @author pearson
 */
public class Actor extends TeamMember {

    public Actor(String name) {
        super(name);
        this.name = name;
        this.position = Position.Actor;
    }

    @Override
    public void setPosition(Position position) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String toString() {
        return "Ator: " + name;
    }
    
    
    
}
