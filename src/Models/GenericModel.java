/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author pearson
 */
public abstract class GenericModel implements TableModel {
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String getFirstColumn() {
        return name;
    }

    @Override
    public String getSecondColumn() {
        return null;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
    
    
}
