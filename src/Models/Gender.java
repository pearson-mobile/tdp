/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.UUID;

/**
 * Gender é um possível enum
 * @author pearson
 */
public class Gender extends GenericModel {

    public Gender(String name) {
        this.name = name;
    }

    @Override
    public String getSecondColumn() {
        return null;
    }
    
    @Override
    public boolean equals(Object object) {
        Gender gender = (Gender) object;
        
        if (gender == null) {
            return false;
        }
        
        if(gender.name == this.name) {
            return true;
        }
        
        
        return false;
    }    
}
