/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 * Position é um possível enum
 * @author pearson
 */
public enum Position {
    Actor("Ator"),
    Director("Diretor"),
    Producer("Produtor"),
    Writer("Roteirista"),
    Edtor("Editor");
    
    private final String text;

    /**
     * @param text
     */
    private Position(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
