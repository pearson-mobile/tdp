/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author pearson
 */
public interface TableModel {
    public String getSecondColumn();
    public String getFirstColumn();
}
