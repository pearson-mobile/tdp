/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Gender;
import Models.TableModel;

/**
 *
 * @author pearson
 */
public class GendersController extends GenericListController {
    
    public GendersController() {
        init();
        
    }
    
    private static void init() {
        selection = new Gender("Ação");
        values = new TableModel[] {
 
            selection,
            new Gender("Animação"),
            new Gender("Aventura"),
            new Gender("Chanchada"),
            new Gender("Cinema catástrofe"),
            new Gender("Comédia"),
            new Gender("Comédia romântica"),
            new Gender("Comédia dramática"),
            new Gender("Comédia de ação"),
            new Gender("Cult"),
            new Gender("Dança"),
            new Gender("Documentários"),
            new Gender("Drama"),
            new Gender("Espionagem"),
            new Gender("Fantasia"),
            new Gender("Faroeste (ou western)"),
            new Gender("Ficção científica"),
            new Gender("Franchise/Séries"),
            new Gender("Guerra"),
            new Gender("Machinima"),
            new Gender("Masala"),
            new Gender("Musical"),
            new Gender("Filme noir"),
            new Gender("Policial"),
            new Gender("Romance"),
            new Gender("Suspense"),
            new Gender("Terror (ou horror)"),
            new Gender("Trash")
        
        };
    }
    
    public static boolean isValidGender(Gender gender) {
        
        init();
        
        for(TableModel object: values) {
            Gender validGender = (Gender)object;
            if(gender.equals(validGender)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    protected TableModel getObjectFromString(String string) {
        
        init();
        
        Gender gender = new Gender(string);
        if(isValidGender(gender)) {
            return gender;
        }
        return null;
    }
    
}
