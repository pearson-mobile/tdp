/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Movie;

/**
 *
 * @author pearson
 */
public class TeamController   extends GenericTableController{
    
    private MoviesController moviesController = null;
    
    private static TeamController sharedInstance = null;

    public static synchronized TeamController getSharedInstance() {
        
        if(sharedInstance == null) {
            sharedInstance = new TeamController();
        }
        
        return sharedInstance;
    }
    
    private TeamController() {
        super();
        moviesController = MoviesController.getSharedInstance();
        list = ((Movie) moviesController.getSelectedObject()).getTeam();
        tableModel.list = list;
        tableModel.setCols(new String[] {"Nome", "Cargo"});
        
        updateRows();
    }

    @Override
    protected void doAdditionalUpdated() {
        ((Movie) moviesController.getSelectedObject()).setTeam(list);
        list = ((Movie) moviesController.getSelectedObject()).getTeam();
        tableModel.list = list;
    }
    

}
