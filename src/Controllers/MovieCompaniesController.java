/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Movie;

/**
 *
 * @author pearson
 */
public class MovieCompaniesController  extends GenericTableController{
    
    private MoviesController moviesController = null;
    
    private static MovieCompaniesController sharedInstance = null;

    public static synchronized MovieCompaniesController getSharedInstance() {
        
        if(sharedInstance == null) {
            sharedInstance = new MovieCompaniesController();
        }
        
        return sharedInstance;
    }
    
    private MovieCompaniesController() {
        super();
        moviesController = MoviesController.getSharedInstance();
        list = ((Movie) moviesController.getSelectedObject()).getCompanies();
        tableModel.list = list;
        tableModel.setCols(new String[] {"Nome", "País"});
        
        updateRows();
    }

    @Override
    protected void doAdditionalUpdated() {
        ((Movie) moviesController.getSelectedObject()).setCompanies(list);
        list = ((Movie) moviesController.getSelectedObject()).getCompanies();
        tableModel.list = list;
    }

}

