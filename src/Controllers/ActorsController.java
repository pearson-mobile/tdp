/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Actor;
import Models.TeamMember;
import Models.Position;
import Models.TableModel;
import java.util.ArrayList;

/**
 *
 * @author pearson
 */
public class ActorsController extends GenericListController {
    
    public static ArrayList<TableModel> list = new ArrayList<>();
    
    
    public ActorsController() {
        
        setActorsList();
        
    }
    
    public static void setActorsList() {
        
        ArrayList<TableModel> personList = PeopleController.getSharedInstance().list;
        list = new ArrayList<>();
        for(TableModel object: personList) {
            TeamMember person = (TeamMember) object;
            
            if(person.getPosition().equals(Position.Actor)){
                list.add(person);
            }
        }
        
        if(!list.isEmpty()) {
            selection = list.get(0);
        }
        
        setValuesFromObject(list);
    }
    
    @Override
    public TableModel getObjectFromString(String string) { 
        for(TableModel object: list) {
            TeamMember actor = (TeamMember) object;
            
            if(actor.getName().equals(string)) {
                return actor;
            }
        }
        
        return null;
    }
    
    
}
