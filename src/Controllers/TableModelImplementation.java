/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.TableModel;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author pearson
 */
public class TableModelImplementation extends AbstractTableModel {

    
    // Atributos para tabela
    private ArrayList rows = new ArrayList<>();
    private String[] cols;
    
    // Lista de objetos salva
    public ArrayList<TableModel> list = new ArrayList<>();

    // Métodos de AbstractTableModel

    public ArrayList getRows() {
        return rows;
    }

    public void setRows(ArrayList rows) {
        this.rows = rows;
    }

    public String[] getCols() {
        return cols;
    }

    public void setCols(String[] cols) {
        this.cols = cols;
    }


    public void updateRows() {
        rows = new ArrayList();
        
        if (list.isEmpty()) {
            return;
        }
        
        for(TableModel object: list) {
            if(object.getSecondColumn() != null) {
                rows.add(new Object[] {object.getFirstColumn(), object.getSecondColumn()});
            }else{
                rows.add(new Object[] {object.getFirstColumn()});
            }
            
        }
        
    }
    
    @Override
    public int getColumnCount() {
        return cols.length;
    }
    
    @Override
    public int getRowCount() {
        return rows.size();
    }
    
    @Override
    public String getColumnName(int numCol) {
        return cols[numCol];
    }
    
    @Override
    public Object getValueAt(int numRow, int numCol) {
        Object[] row = (Object[]) getRows().get(numRow);
        return row[numCol];
    }
    
}
