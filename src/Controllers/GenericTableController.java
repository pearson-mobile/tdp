/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.TableModel;
import java.util.ArrayList;

/**
 *
 * @author pearson
 */
public abstract class GenericTableController {
    
    protected ArrayList<TableModel> list;
    protected TableModelImplementation tableModel = null;
    protected TableModel selectedObject;

    public GenericTableController() {
        list = new ArrayList<>();
        tableModel = new TableModelImplementation();
        tableModel.list = list;
        updateRows();
    }
    
    

    public void save(TableModel newObject) {

        if(list == null) {
            list = new ArrayList<>();
        }
        list.add(newObject);
        updateRows();
        doAdditionalUpdated();
    }
    
    public void edit(TableModel objectToBeEdited) {
        
        for(TableModel object: list) {
            if(objectToBeEdited.equals(object)) {
                list.set(list.indexOf(object), objectToBeEdited);
                break;
            }
        }
        
        updateRows();
        doAdditionalUpdated();
    }
    
    public void delete(TableModel objectToBeDeleted) {
        
        for(TableModel object: list) {
            if(objectToBeDeleted.equals(object)) {
                list.remove(object);
                break;
            }
        }
        updateRows();
        doAdditionalUpdated();
    }
    
    protected abstract void doAdditionalUpdated();
    
    protected final void updateRows() {
        tableModel.list = list; 
        tableModel.updateRows();
    }
    
    public TableModel getSelectedObject() {
        return selectedObject;
    }

    public void setSelectedObject(TableModel selectedObject) {
        this.selectedObject = selectedObject;
    }

    public ArrayList<TableModel> getList() {
        return list;
    }

    public void setList(ArrayList<TableModel> list) {
        tableModel.list = list;
        this.list = list;
        updateRows();
    }

    public TableModelImplementation getTableModel() {
        TableModelImplementation newModel = new TableModelImplementation();
        newModel.list = list;
        newModel.setRows(tableModel.getRows());
        newModel.setCols(tableModel.getCols());
        return newModel;
    }
    
    
}
