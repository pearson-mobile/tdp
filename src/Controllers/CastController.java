/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Movie;
import Models.TableModel;
import java.util.ArrayList;

/**
 *
 * @author pearson
 */
public class CastController extends GenericTableController{
    
    private MoviesController moviesController = null;
    
    private static CastController sharedInstance = null;

    public static synchronized CastController getSharedInstance() {
        
        if(sharedInstance == null) {
            sharedInstance = new CastController();
        }
        
        return sharedInstance;
    }
    
    private CastController() {
        super();
        moviesController = MoviesController.getSharedInstance();
        list = ((Movie) moviesController.getSelectedObject()).getCast();
        tableModel.list = list;
        tableModel.setCols(new String[] {"Nome", "Personagem"});
        
        updateRows();
    }

    @Override
    protected void doAdditionalUpdated() {
        ((Movie) moviesController.getSelectedObject()).setCast(list);
        list = ((Movie) moviesController.getSelectedObject()).getCast();
        tableModel.list = list;
        
        updateRows();
    }

}
