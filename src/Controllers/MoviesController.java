/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Gender;
import Models.Movie;

/**
 *
 * @author pearson
 */
public class MoviesController extends GenericTableController{
    private static MoviesController sharedInstance = null;

    public static synchronized MoviesController getSharedInstance() {
        
        if(sharedInstance == null) {
            sharedInstance = new MoviesController();
        }
        
        return sharedInstance;
    }
    
    private MoviesController() {
        super();
        
        
        Movie movie = new Movie("Teste", "Teste", "Teste", "11/11/1111", new Gender("Ação"), "R$ 1,00", "R$ 2,00", "130");
        
        list.add(movie);
        
        movie = new Movie("Teste2", "Teste2", "Teste", "11/11/1112", new Gender("Comédia"), "R$ 1,00", "R$ 2,00", "130");
        
        list.add(movie);
        
        tableModel.list = list;
        tableModel.setCols(new String[] {"Nome", "Ano"});
        
        updateRows();
    }

    @Override
    protected void doAdditionalUpdated() {
        // Não precisa de nenhuma atualização adicional
    }
    
    
    
}
