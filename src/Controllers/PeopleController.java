/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.TeamMember;
import Models.Position;
import Models.TableModel;
import java.util.ArrayList;

/**
 *
 * @author pearson
 */
public class PeopleController extends GenericTableController {
    
    private static PeopleController sharedInstance = null;

    public static synchronized PeopleController getSharedInstance() {
        
        if(sharedInstance == null) {
            sharedInstance = new PeopleController();
        }
        
        return sharedInstance;
    }
    
    private PeopleController() {
        super();
        
        list.add(new TeamMember("Leonardo Di Caprio", Position.Actor));
        list.add(new TeamMember("Scarlett Johansson", Position.Actor));
        list.add(new TeamMember("Sofia Coppola", Position.Writer));
        list.add(new TeamMember("Will Smith", Position.Actor));
        list.add(new TeamMember("Steven Spielberg", Position.Director));
        
        
        tableModel.list = list;
        tableModel.setCols(new String[] {"Nome"});
        
        updateRows();
    }

    @Override
    protected void doAdditionalUpdated() { }
    
    public Object[] getNonActorComboboxModel() {
        ArrayList<TableModel> people = new ArrayList<>();
        
        for(TableModel object: list) {
            TeamMember person = (TeamMember) object;
            if(person.getPosition() != Position.Actor) {
                people.add(person);
            }
        }
        
        return people.toArray();
    }
    
}
