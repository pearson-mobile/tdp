/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Company;
import Models.Country;
import Models.TableModel;
import java.util.ArrayList;

/**
 *
 * @author pearson
 */
public class CompaniesController extends GenericTableController {
    
    private static CompaniesController sharedInstance = null;

    public static synchronized CompaniesController getSharedInstance() {
        
        if(sharedInstance == null) {
            sharedInstance = new CompaniesController();
        }
        
        return sharedInstance;
    }
    
    private CompaniesController() {
        super();
        CountryController.init();
        list.add(new Company("20th Century Fox", (Country) CountryController.values[0]));
        list.add(new Company("Fox", (Country) CountryController.values[0]));
        list.add(new Company("Sony", (Country) CountryController.values[15]));
        
        tableModel.list = list;
        tableModel.setCols(new String[] {"Nome", "País"});
        
        updateRows();
    }
    
    public Object[] getComboboxModel() {
        
        return list.toArray();
    }
    
    @Override
    protected void doAdditionalUpdated() { }
    
    
    
}
