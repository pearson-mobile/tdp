/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.TableModel;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;


public abstract class GenericListController extends AbstractListModel implements ComboBoxModel {

    static TableModel[] values;
    static TableModel selection = null;
    
    protected abstract TableModel getObjectFromString(String string);

    public static void setValuesFromObject(ArrayList<TableModel> array) {
        TableModel[] aux = new TableModel[array.size()];
        
        Iterator<TableModel> iterator = array.iterator();
        for (int i = 0; i < aux.length; i++) {
            aux[i] = iterator.next();
        }
        
        values = aux;
    }
    
    @Override
    public Object getElementAt(int index) {
        return values[index];
    }

    @Override
    public int getSize() {
        return values.length;
    }

    @Override
    public void setSelectedItem(Object anItem) {        
        selection = (TableModel) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selection;
    }
    
}
