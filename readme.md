# Unidade 3

## Aula 1

### O Prazo
Não há gente o suficiente para que o projeto seja finalizado dentro do prazo! Por enquanto todos os desenvolvedores estão alocados em outros projetos.
Um dos maiores problemas em um projeto de software. Um código profissional e com qualidade necessita de tempo e dedicação. Quando há um prazo apertado, geralmente contrata-se mais pessoas para desenvolver, porém as vezes um projeto não suporta tanta gente.
Quando há um excesso de desenvolvedores, os trabalhos podem ser conflitantes e a paralelização de atividades torna-se mais difícil.
Outra solução para atender um prazo apertado é diminuir a quantidade de artefatos.

### Artefatos
Um artefato é qualquer documento ou algo que dê valor ao projeto. A próxima etapa seria a arquitetural. O arquiteto já identificou as classes, métodos e atributos e agora deveria colocá-las em um diagrama. Como estamos com o prazo apertado, vamos pular esta etapa e deixá-la para o final.
Para suprir a necessidade de mais desenvolvedores, você irá programar.

---
### Tarefa
Implemente as classes identificadas. Teste todos os construtores, getters e setters.

### Solução
Vide código